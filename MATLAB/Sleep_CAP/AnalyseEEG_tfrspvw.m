function [ ] = AnalyseEEG_tfrspvw(file, channel)
  % AnalyseEEG_tfrspvw	 A script to perform spwvd transform on all
  %                      epochs in an EEG channel.
  %	[ ] = AnalyseEEG_tfrspvw(file, channel)

  % Copyright (c) 2014, Nick Palmius (University of Oxford)
  % All rights reserved.
  %
  % Redistribution and use in source and binary forms, with or without
  % modification, are permitted provided that the following conditions are
  % met:
  %
  % 1. Redistributions of source code must retain the above copyright
  %    notice, this list of conditions and the following disclaimer.
  %
  % 2. Redistributions in binary form must reproduce the above copyright
  %    notice, this list of conditions and the following disclaimer in the
  %    documentation and/or other materials provided with the distribution.
  %
  % 3. Neither the name of the University of Oxford nor the names of its
  %    contributors may be used to endorse or promote products derived
  %    from this software without specific prior written permission.
  %
  % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  % "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  % LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  % A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  % HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  % SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  % LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  % DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  % THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  % (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  % OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  %	Contact: npalmius@googlemail.com
  %	Originally written by Nick Palmius, 14-Mar-2014
  
  names = {'Awake', 'S1', 'S2', 'S3', 'S4', 'REM', '', 'Movement'};
  
  fprintf(' Loading data...\n');
  
  data = open([getenv('MATLAB_DATA') '/Sleep_CAP/' file '.' channel '.mat']);
  hyp  = open([getenv('MATLAB_DATA') '/Sleep_CAP/' file '.hyp.mat']);
  
  data_sr = data.hdr.samples / data.hdr.duration;
  
  epochs = floor(length(data.eeg) / (data_sr * 30));
  
  fprintf(' Analysing data...\n');
  
  for i = 1:epochs
    fprintf('  epoch %i of %i (%.2f%%)\n', i, epochs, (i/epochs) * 100);
    [tfr, ~, tfr_sum] = AnalyseEpoch_tfrspvw(data.eeg, data.hdr, i, ['File ' file ', Channel ' channel ', Epoch ' num2str(i) ' (' names{hyp.hyp(i,1) + 1} ')']);
    
    if i == 1
      tfr_sum_all = zeros([epochs length(tfr_sum)]);
    end
    
    tfr_sum_all(i,:) = tfr_sum;
    
    eval (['save ' getenv('MATLAB_DATA') '/Sleep_CAP/tfr/' file '.' channel '.' num2str(i) '.' names{hyp.hyp(i,1) + 1} '.mat tfr tfr_sum'])
    eval (['save ' getenv('MATLAB_DATA') '/Sleep_CAP/tfr/' file '.' channel '.all.mat tfr_sum_all'])
    
    saveas(gcf, [getenv('MATLAB_DATA') '/Sleep_CAP/tfr/' file '.' channel '.' num2str(i) '.' names{hyp.hyp(i,1) + 1} '.png'], 'png');
    %saveas(gcf, [getenv('MATLAB_DATA') '/Sleep_CAP/tfr/' file '.' channel '.' num2str(i) '.' names{hyp.hyp(i,1) + 1} '.fig'], 'fig');
    %saveas(gcf, [getenv('MATLAB_DATA') '/Sleep_CAP/tfr/' file '.' channel '.' num2str(i) '.' names{hyp.hyp(i,1) + 1} '.epsc'], 'epsc');
    
    close all;
  end