function [ ] = AnalyseSleepStageDuration_Thanasis()
  % AnalyseSleepStageDuration_Thanasis A Function to analyse sleep stage
  % duration in sleep data processed by Thanasis (Sleep_CAP_Nick.mat)
  %	[ ] = LoadSleepData_Thanasis() 

  % Copyright (c) 2013, Nick Palmius (University of Oxford)
  % All rights reserved.
  %
  % Redistribution and use in source and binary forms, with or without
  % modification, are permitted provided that the following conditions are
  % met:
  %
  % 1. Redistributions of source code must retain the above copyright
  %    notice, this list of conditions and the following disclaimer.
  %
  % 2. Redistributions in binary form must reproduce the above copyright
  %    notice, this list of conditions and the following disclaimer in the
  %    documentation and/or other materials provided with the distribution.
  %
  % 3. Neither the name of the University of Oxford nor the names of its
  %    contributors may be used to endorse or promote products derived
  %    from this software without specific prior written permission.
  %
  % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  % "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  % LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  % A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  % HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  % SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  % LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  % DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  % THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  % (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  % OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  %	Contact: npalmius@googlemail.com
  %	Originally written by Nick Palmius, 01-Dec-2013
  
  currentdir = pwd;
  cd(fileparts(mfilename('fullpath')));

  close All;
  
  [ Sleep, Sleep_names ] = LoadSleepData_Thanasis;
  
  Sleep_names'
  
  Sleep_healthy    = Sleep(Sleep(:,6) == 0, :);
  Sleep_nocturnal  = Sleep(Sleep(:,6) == 1, :);
  Sleep_bruxism    = Sleep(Sleep(:,6) == 2, :);
  Sleep_insomnia   = Sleep(Sleep(:,6) == 3, :);
  Sleep_narcolepsy = Sleep(Sleep(:,6) == 4, :);
  
  %hist(Sleep_healthy(Sleep_healthy(:,1) == min(Sleep_healthy(:,1)), 7), 0:1:7)
  
  stage_length = {[] [] [] [] [] [] []};
  stage_length_n = {[] [] [] [] [] [] []};
  
  state_id = [0 1 2 3 4 5 7];
  
  for i=unique(Sleep_healthy(:,1))'
    Sleep_subject = Sleep_healthy(Sleep_healthy(:,1) == i, :);
    
    for j=1:7
      % Append to the stage_length vector an entry for the time of each
      % duration in a state.
      %
      % Find indexes in required state.
      %   find(Sleep_subject(:,7)' == j)
      % The number of indexes between the last occurrence of the state and
      % the current. If this is one then no transition has occurred, if
      % this is greater than one then time has been spent in another state.
      %   diff([indexes inf]
      % Find indexes of states at which transitions have been made.
      %   find(state_differences) > 1)
      % The differences between these indexes will now contain the length
      % of time spent in the state.
      %   diff([0 state_differences_greater_1])
      % Finally append these lengths to the array.
      lengths_n = diff([0 find(diff([find(Sleep_subject(:,7)' == state_id(j)) inf]) > 1)]);
      stage_length_n{j} = [stage_length_n{j} lengths_n];
      stage_length{j} = [stage_length{j} (lengths_n * 30)];
    end
  end
  NBINS = [0:120:5000] + (120 / 2);
  figure
  hist(stage_length{1}, NBINS)
  format_figure;
  a = axis; a([1 2]) = [0 5000]; axis(a)
  title('Awake');
  xlabel('Sleep stage duration (s)')
  ylabel('Number of epochs (n)')
  save_figure('Duration_W', [{'Sleep_CAP'} {'Duration_Distributions'}]);
  figure
  hist(stage_length{2}, NBINS)
  format_figure;
  a = axis; a([1 2]) = [0 5000]; axis(a)
  title('Sleep stage 1');
  xlabel('Sleep stage duration (s)')
  ylabel('Number of epochs (n)')
  save_figure('Duration_N1', [{'Sleep_CAP'} {'Duration_Distributions'}]);
  figure
  hist(stage_length{3}, NBINS)
  format_figure;
  a = axis; a([1 2]) = [0 5000]; axis(a)
  title('Sleep stage 2');
  xlabel('Sleep stage duration (s)')
  ylabel('Number of epochs (n)')
  save_figure('Duration_N2', [{'Sleep_CAP'} {'Duration_Distributions'}]);
  figure
  hist(stage_length{4}, NBINS)
  format_figure;
  a = axis; a([1 2]) = [0 5000]; axis(a)
  title('Sleep stage 3');
  xlabel('Sleep stage duration (s)')
  ylabel('Number of epochs (n)')
  save_figure('Duration_N3', [{'Sleep_CAP'} {'Duration_Distributions'}]);
  figure
  hist(stage_length{5}, NBINS)
  format_figure;
  a = axis; a([1 2]) = [0 5000]; axis(a)
  title('Sleep stage 4');
  xlabel('Sleep stage duration (s)')
  ylabel('Number of epochs (n)')
  save_figure('Duration_N4', [{'Sleep_CAP'} {'Duration_Distributions'}]);
  figure
  hist(stage_length{6}, NBINS)
  format_figure;
  a = axis; a([1 2]) = [0 5000]; axis(a)
  title('REM');
  xlabel('Sleep stage duration (s)')
  ylabel('Number of epochs (n)')
  save_figure('Duration_REM', [{'Sleep_CAP'} {'Duration_Distributions'}]);
  figure
  hist(stage_length{7}, NBINS)
  format_figure;
  a = axis; a([1 2]) = [0 5000]; axis(a)
  title('Movement');
  xlabel('Sleep stage duration (s)')
  ylabel('Number of epochs (n)')
  save_figure('Duration_Movement', [{'Sleep_CAP'} {'Duration_Distributions'}]);
  
  save_data('distributions.mat', [{'Sleep_CAP'} {'Duration_Distributions'}], 'stage_length', 'stage_length_n');
  
  cd(currentdir);

end