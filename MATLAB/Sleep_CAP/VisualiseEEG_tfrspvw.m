function [ ] = VisualiseEEG_tfrspvw(file, channel, epochs)
  % VisualiseEEG_tfrspvw	 A script to visualise the tfr of an EEG
  %                        signal for publication.
  %	[ ] = VisualiseEEG_tfrspvw(file, channel, epochs)

  % Copyright (c) 2014, Nick Palmius (University of Oxford)
  % All rights reserved.
  %
  % Redistribution and use in source and binary forms, with or without
  % modification, are permitted provided that the following conditions are
  % met:
  %
  % 1. Redistributions of source code must retain the above copyright
  %    notice, this list of conditions and the following disclaimer.
  %
  % 2. Redistributions in binary form must reproduce the above copyright
  %    notice, this list of conditions and the following disclaimer in the
  %    documentation and/or other materials provided with the distribution.
  %
  % 3. Neither the name of the University of Oxford nor the names of its
  %    contributors may be used to endorse or promote products derived
  %    from this software without specific prior written permission.
  %
  % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  % "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  % LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  % A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  % HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  % SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  % LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  % DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  % THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  % (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  % OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  %	Contact: npalmius@googlemail.com
  %	Originally written by Nick Palmius, 17-Mar-2014
  
  close all;
  
  names = {'Awake', 'S1', 'S2', 'S3', 'S4', 'REM', '', 'Movement'};
  
  fprintf(' Loading data...\n');
  
  data = open([getenv('MATLAB_DATA') '/Sleep_CAP/' file '.' channel '.mat']);
  hyp  = open([getenv('MATLAB_DATA') '/Sleep_CAP/' file '.hyp.mat']);
  
  data_sr = data.hdr.samples / data.hdr.duration;
  
  fprintf(' Analysing data...\n');
  
  j = 1;
  for i = epochs
    fprintf('  epoch %i of %i (%.2f%%)\n', j, length(epochs), (j/length(epochs)) * 100);
    VisualiseEpoch_tfrspvw(data.eeg, data.hdr, i, '');
    
    saveas(gcf, [getenv('MATLAB_FIGURES') '/Sleep_CAP/tfr/' file '_' channel '_' num2str(i) '_' names{hyp.hyp(i,1) + 1} '.png'], 'png');
    saveas(gcf, [getenv('MATLAB_FIGURES') '/Sleep_CAP/tfr/' file '_' channel '_' num2str(i) '_' names{hyp.hyp(i,1) + 1} '.fig'], 'fig');
    saveas(gcf, [getenv('MATLAB_FIGURES') '/Sleep_CAP/tfr/' file '_' channel '_' num2str(i) '_' names{hyp.hyp(i,1) + 1} '.eps'], 'epsc');
    
    %close all;
    
    j = j + 1;
  end