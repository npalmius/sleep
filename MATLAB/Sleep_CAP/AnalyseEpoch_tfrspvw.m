function [ data_epoch_ds_tfr_scaled, data_epoch_ds, data_epoch_ds_tfr_sum_norm, epoch_ds_t, data_epoch_ds_f ] = AnalyseEpoch_tfrspvw(eeg_data, eeg_hdr, epoch, figure_title)
  % AnalyseEpoch_tfrspvw	 A script to perform spwvd transform on all a
  %                        single epoch in an EEG channel.
  %	[ data_epoch_ds_tfr_scaled, data_epoch_ds, 
  %   data_epoch_ds_tfr_sum_norm, epoch_ds_t, 
  %   data_epoch_ds_f ] = AnalyseEpoch_tfrspvw(eeg_data, eeg_hdr, epoch, figure_title)

  % Copyright (c) 2014, Nick Palmius (University of Oxford)
  % All rights reserved.
  %
  % Redistribution and use in source and binary forms, with or without
  % modification, are permitted provided that the following conditions are
  % met:
  %
  % 1. Redistributions of source code must retain the above copyright
  %    notice, this list of conditions and the following disclaimer.
  %
  % 2. Redistributions in binary form must reproduce the above copyright
  %    notice, this list of conditions and the following disclaimer in the
  %    documentation and/or other materials provided with the distribution.
  %
  % 3. Neither the name of the University of Oxford nor the names of its
  %    contributors may be used to endorse or promote products derived
  %    from this software without specific prior written permission.
  %
  % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  % "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  % LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  % A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  % HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  % SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  % LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  % DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  % THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  % (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  % OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  %	Contact: npalmius@googlemail.com
  %	Originally written by Nick Palmius, 14-Mar-2014
  
  data_sr = eeg_hdr.samples / eeg_hdr.duration;
  
  epoch_idx = (((epoch - 1) * 30 * data_sr) + 1):(epoch * 30 * data_sr);
  
  epoch_t = (epoch_idx - 1) / data_sr;
  
  data_epoch = eeg_data(epoch_idx, 1);
  
  data_epoch_spline = spline(epoch_t, data_epoch);
  
  epoch_ds_t = min(epoch_t):(1/50):max(epoch_t);
  
  data_epoch_ds = ppval(data_epoch_spline, epoch_ds_t);
  
  [data_epoch_ds_tfr, data_epoch_ds_f] = tfrspwv(data_epoch_ds');
  
  data_epoch_ds_tfr_scaled = (data_epoch_ds_tfr-min(data_epoch_ds_tfr(:))) ./ (max(data_epoch_ds_tfr(:)-min(data_epoch_ds_tfr(:))));
  
  data_epoch_ds_tfr_sum = sum(data_epoch_ds_tfr_scaled, 2);
  
  data_epoch_ds_tfr_sum_norm = data_epoch_ds_tfr_sum *(1 / norm(data_epoch_ds_tfr_sum));
  
  figure;
  
  h_data = subplot(7, 7, 2:7);
  
  plot(epoch_ds_t, data_epoch_ds, 'LineWidth', 2)
  
  h_tfr = subplot(7, 7, [9:14, 16:21, 23:28, 30:35, 37:42, 44:49]);
  
  imagesc(epoch_ds_t, data_epoch_ds_f, data_epoch_ds_tfr_scaled);
  
  h_tfr_sum = subplot(7, 7, [8, 15, 22, 29, 36, 43]);
  
  plot(data_epoch_ds_tfr_sum_norm, data_epoch_ds_f, 'LineWidth', 2);
  
  set(h_tfr_sum,'YDir','Reverse');
  
  xlabel(h_tfr, 'Epoch time (seconds)', 'FontSize', 15);
  ylabel(h_tfr_sum, 'Frequency', 'FontSize', 15);
  
  title(h_data, figure_title, 'FontSize', 20);
  
  set(h_data, 'FontSize', 12)
  set(h_tfr, 'FontSize', 12)
  set(h_tfr_sum, 'FontSize', 12)
  
  a = axis(h_data);
  a(3) = eeg_hdr.physicalMin;
  a(4) = eeg_hdr.physicalMax;
  axis(h_data, a);
  ylabel(h_data, eeg_hdr.units, 'FontSize', 15);