function [ ] = AnalyseSleepStageObservationsDistributions_tfrspvw()
  % AnalyseSleepStageDurationDistributions A Function to analyse sleep
  % stage observation distributions for tfrspvw.
  %	[ ] = AnalyseSleepStageObservationsDistributions_tfrspvw() 

  % Copyright (c) 2014, Nick Palmius (University of Oxford)
  % All rights reserved.
  %
  % Redistribution and use in source and binary forms, with or without
  % modification, are permitted provided that the following conditions are
  % met:
  %
  % 1. Redistributions of source code must retain the above copyright
  %    notice, this list of conditions and the following disclaimer.
  %
  % 2. Redistributions in binary form must reproduce the above copyright
  %    notice, this list of conditions and the following disclaimer in the
  %    documentation and/or other materials provided with the distribution.
  %
  % 3. Neither the name of the University of Oxford nor the names of its
  %    contributors may be used to endorse or promote products derived
  %    from this software without specific prior written permission.
  %
  % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  % "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  % LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  % A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  % HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  % SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  % LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  % DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  % THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  % (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  % OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  %	Contact: npalmius@googlemail.com
  %	Originally written by Nick Palmius, 18-May-2014
  
  currentdir = pwd;
  cd(fileparts(mfilename('fullpath')));

  close All;
  
  files = what('~/Research_sync/Data/Sleep_CAP/tfr/');
  
  class_idx = cell(8,1);
  
  FEATURES = 11;
  
  F_total = zeros(0, FEATURES);
  
  for file = 1:length(files.mat)
    filename = files.mat{file};

    tfr_data = load(['~/Research_sync/Data/Sleep_CAP/tfr/' filename]);
    hyp_data = load(['~/Research_sync/Data/Sleep_CAP/' regexprep(filename, '\.[A-Z0-9]+\.all', '.hyp')]);

    hyp = hyp_data.hyp(:,1);

    tfr_sum_all = tfr_data.tfr_sum_all;

    tfr_sum_all_f = 50/2*linspace(0,1,size(tfr_sum_all, 2)/2+1);
    tfr_sum_all_t = (0:(size(tfr_sum_all, 1) - 1)) / 2 / 60;

    tfr_sum_sub = tfr_sum_all(:,1:length(tfr_sum_all_f));
    tfr_sum_sub_f = tfr_sum_all_f;%(1:length(tfr_sum_all_f));
    tfr_sum_F = zeros(size(tfr_sum_sub));

    num = size(tfr_sum_sub, 1);

    F = zeros(num, FEATURES);

    for i = 1:num
      iMin = max(1, i - 2);
      iMax = min(num, i + 2);
      F(i, 1)  = mean(mean(tfr_sum_sub(iMin:iMax, 1:15)));
      F(i, 2)  = mean(mean(tfr_sum_sub(iMin:iMax, 16:30)));
      F(i, 3)  = mean(mean(tfr_sum_sub(iMin:iMax, 31:45)));
      F(i, 4)  = mean(mean(tfr_sum_sub(iMin:iMax, 46:60)));
      F(i, 5)  = mean(mean(tfr_sum_sub(iMin:iMax, 61:75)));
      F(i, 6)  = mean(mean(tfr_sum_sub(iMin:iMax, 76:90)));
      F(i, 7)  = mean(mean(tfr_sum_sub(iMin:iMax, 91:105)));
      F(i, 8)  = mean(mean(tfr_sum_sub(iMin:iMax, 106:120)));
      F(i, 9)  = mean(mean(tfr_sum_sub(iMin:iMax, 121:240)));
      F(i, 10) = mean(mean(tfr_sum_sub(iMin:iMax, 241:390)));
      F(i, 11) = mean(mean(tfr_sum_sub(iMin:iMax, 391:end)));

      class_idx{hyp(i) + 1} = [class_idx{hyp(i) + 1} (i + size(F_total, 1))];
    end

    F_total = [F_total; F];
    
    imagesc(F);
  end
  
  class_idx(7) = [];
  
  observation_mean = zeros(7,FEATURES);
  observation_std  = zeros(7,FEATURES);
  
  for j=1:11
      figure
      for i=1:7
        if ~isempty(class_idx{i})
          hist(F_total(class_idx{i}, j))
          mu = mean(F_total(class_idx{i}, j));
          sigma = std(F_total(class_idx{i}, j));
          observation_mean(i, j) = mu;
          observation_std(i, j) = sigma;
          hold on
          plot([mu mu], [0 3500]);
        end
      end
      hold off
  end
  
  save_data('observations.mat', [{'Sleep_CAP'} {'Observation_Distributions'}], 'observation_mean', 'observation_std');
  
  cd(currentdir);

end