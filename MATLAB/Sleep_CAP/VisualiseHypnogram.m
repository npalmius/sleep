function [ ] = VisualiseHypnogram(file, channel)
  % VisualiseHypnogram A script to visualise the whole hypnogram of an EEG
  %                    channel for publication.
  %	[ ] = VisualiseHypnogram() 

  % Copyright (c) 2014, Nick Palmius (University of Oxford)
  % All rights reserved.
  %
  % Redistribution and use in source and binary forms, with or without
  % modification, are permitted provided that the following conditions are
  % met:
  %
  % 1. Redistributions of source code must retain the above copyright
  %    notice, this list of conditions and the following disclaimer.
  %
  % 2. Redistributions in binary form must reproduce the above copyright
  %    notice, this list of conditions and the following disclaimer in the
  %    documentation and/or other materials provided with the distribution.
  %
  % 3. Neither the name of the University of Oxford nor the names of its
  %    contributors may be used to endorse or promote products derived
  %    from this software without specific prior written permission.
  %
  % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  % "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  % LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  % A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  % HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  % SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  % LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  % DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  % THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  % (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  % OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  %	Contact: npalmius@googlemail.com
  %	Originally written by Nick Palmius, 15-Mar-2014
  
  data_in = open([getenv('MATLAB_DATA') '/Sleep_CAP/tfr/' file '.' channel '.all.mat']);
  hyp_in = open([getenv('MATLAB_DATA') '/Sleep_CAP/' file '.hyp.mat']);
  
  tfr_sum_all = data_in.tfr_sum_all;
  hyp = hyp_in.hyp(:,1);
  
  close all;
  
  tfr_sum_all_f = 50/2*linspace(0,1,size(tfr_sum_all, 2)/2+1);
  tfr_sum_all_t = (0:(size(tfr_sum_all, 1) - 1)) / 2 / 60;
  
  tfr_sum_sub = tfr_sum_all(:,1:length(tfr_sum_all_f));
  tfr_sum_sub_f = tfr_sum_all_f;%(1:length(tfr_sum_all_f));
  tfr_sum_F = zeros(size(tfr_sum_sub));
  
  num = size(tfr_sum_sub, 1);
  
  FEATURES = 11;
  
  F = zeros(num, FEATURES);
  
  class_idx = cell(8,1);
  
  for i = 1:num
    iMin = max(1, i - 0);
    iMax = min(num, i + 0);
    [F(i, 1)  tfr_sum_F(i, 1:15)]    = split2( mean(mean(tfr_sum_sub(iMin:iMax, 1:15)))    );
    [F(i, 2)  tfr_sum_F(i, 16:30)]   = split2( mean(mean(tfr_sum_sub(iMin:iMax, 16:30)))   );
    [F(i, 3)  tfr_sum_F(i, 31:45)]   = split2( mean(mean(tfr_sum_sub(iMin:iMax, 31:45)))   );
    [F(i, 4)  tfr_sum_F(i, 46:60)]   = split2( mean(mean(tfr_sum_sub(iMin:iMax, 46:60)))   );
    [F(i, 5)  tfr_sum_F(i, 61:75)]   = split2( mean(mean(tfr_sum_sub(iMin:iMax, 61:75)))   );
    [F(i, 6)  tfr_sum_F(i, 76:90)]   = split2( mean(mean(tfr_sum_sub(iMin:iMax, 76:90)))   );
    [F(i, 7)  tfr_sum_F(i, 91:105)]  = split2( mean(mean(tfr_sum_sub(iMin:iMax, 91:105)))  );
    [F(i, 8)  tfr_sum_F(i, 106:120)] = split2( mean(mean(tfr_sum_sub(iMin:iMax, 106:120))) );
    [F(i, 9)  tfr_sum_F(i, 121:240)] = split2( mean(mean(tfr_sum_sub(iMin:iMax, 121:240))) );
    [F(i, 10) tfr_sum_F(i, 241:390)] = split2( mean(mean(tfr_sum_sub(iMin:iMax, 241:390))) );
    [F(i, 11) tfr_sum_F(i, 391:end)] = split2( mean(mean(tfr_sum_sub(iMin:iMax, 391:end))) );
    
    class_idx{hyp(i) + 1} = [class_idx{hyp(i) + 1} i];
  end
  
  for i = 1:2
    figure;
    
    h_image = subplot(7, 1, 1:5);
    
    if i == 1
      imagesc(tfr_sum_all_t, tfr_sum_sub_f, tfr_sum_sub');
    else
      imagesc(tfr_sum_all_t, tfr_sum_sub_f, tfr_sum_F');
      
      a = axis;
      a(4) = 15;
      axis(a)
    end
    h_hyp = subplot(7, 1, 6:7);
    
    stairs(tfr_sum_all_t, hyp, '-r');
    a = axis;
    a(2) = max(tfr_sum_all_t);
    axis(a)
    
    set(h_image,'YDir','normal');
    
    ylabel(h_image, 'Frequency (Hz)', 'FontSize', 15);
    
    xlabel(h_hyp, 'Annotation time (hours)', 'FontSize', 15);
    ylabel(h_hyp, 'Sleep stage', 'FontSize', 15);
    set(h_hyp,'YTick',[0 1 2 3 4 5 7]);
    set(h_hyp,'YTickLabel',{'W','S1','S2','S3','S4','R','M'})
    
    set(h_hyp, 'FontSize', 12)
    set(h_image, 'FontSize', 12)
    
    set(h_image,'xtick',[])
    
    saveas(gcf, [getenv('MATLAB_FIGURES') '/Sleep_CAP/tfr/' file '_' channel '_hypnogram_' num2str(i) '.png'], 'png');
    saveas(gcf, [getenv('MATLAB_FIGURES') '/Sleep_CAP/tfr/' file '_' channel '_hypnogram_' num2str(i) '.fig'], 'fig');
    saveas(gcf, [getenv('MATLAB_FIGURES') '/Sleep_CAP/tfr/' file '_' channel '_hypnogram_' num2str(i) '.eps'], 'epsc');
  end
end

function [ a b ] = split2 (val)
  a = val;
  b = val;
end