function [ ] = AnalyseSleepStageTransitions_Thanasis()
  % AnalyseSleepStageTransitions_Thanasis A Function to analyse sleep stage
  % transitions in sleep data processed by Thanasis (Sleep_CAP_Nick.mat)
  %	[ ] = AnalyseSleepStageTransitions_Thanasis() 

  % Copyright (c) 2013, Nick Palmius (University of Oxford)
  % All rights reserved.
  %
  % Redistribution and use in source and binary forms, with or without
  % modification, are permitted provided that the following conditions are
  % met:
  %
  % 1. Redistributions of source code must retain the above copyright
  %    notice, this list of conditions and the following disclaimer.
  %
  % 2. Redistributions in binary form must reproduce the above copyright
  %    notice, this list of conditions and the following disclaimer in the
  %    documentation and/or other materials provided with the distribution.
  %
  % 3. Neither the name of the University of Oxford nor the names of its
  %    contributors may be used to endorse or promote products derived
  %    from this software without specific prior written permission.
  %
  % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  % "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  % LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  % A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  % HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  % SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  % LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  % DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  % THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  % (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  % OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  %	Contact: npalmius@googlemail.com
  %	Originally written by Nick Palmius, 12-Jan-2014
  
  currentdir = pwd;
  cd(fileparts(mfilename('fullpath')));

  close All;
  
  [ Sleep, Sleep_names ] = LoadSleepData_Thanasis;
  
  Sleep_names'
  Sleep_names{7}
  
  Sleep_healthy    = Sleep(Sleep(:,6) == 0, :);
  Sleep_nocturnal  = Sleep(Sleep(:,6) == 1, :);
  Sleep_bruxism    = Sleep(Sleep(:,6) == 2, :);
  Sleep_insomnia   = Sleep(Sleep(:,6) == 3, :);
  Sleep_narcolepsy = Sleep(Sleep(:,6) == 4, :);
  
  %hist(Sleep_healthy(Sleep_healthy(:,1) == min(Sleep_healthy(:,1)), 7), 0:1:7)
  
  stage_transitions = zeros(8, 8);
  
  for i=unique(Sleep_healthy(:,1))'
    Sleep_subject = Sleep_healthy(Sleep_healthy(:,1) == i, :);
    
    for j=1:(length(Sleep_subject(:,7)) - 1)
      stage_transitions(Sleep_subject(j,7)+1, Sleep_subject(j+1,7)+1) = stage_transitions(Sleep_subject(j,7)+1, Sleep_subject(j+1,7)+1) + 1;
    end
  end
  
  stage_transitions(7,:) = [];
  stage_transitions(:,7) = [];
  stage_transitions
  
  stage_transitions_ = zeros(7, 7);
  stage_transitions__ = zeros(7, 7);
  
  for i = 1:7
    transition_row = stage_transitions(i, :);
    stage_transitions_(i, :) = transition_row ./ sum(transition_row);
    transition_row(i) = 0;
    stage_transitions__(i, :) = transition_row ./ sum(transition_row);
  end
  
  stage_transitions_
  stage_transitions__
  
  figure
  stage_transitions_(find(stage_transitions_ == 0)) = 1.01;
  
  imagesc((stage_transitions_))
  
  format_figure
  
  title('Sleep stage transitions');
  ylabel('Current stage');
  xlabel('Next stage');
  set(gca,'XTickLabel',{'W','N1','N2','N3','N4','R','M'})
  set(gca,'YTickLabel',{'W','N1','N2','N3','N4','R','M'})
  
  colormap([flipud(autumn); 1 1 1])
  
  for i = 1:7
    for j = 1:7
      if stage_transitions_(j, i) <= 1
        text(i, j, num2str(round(stage_transitions_(j, i) * 10000)/10000, 2), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FontSize',16)
      end
    end
  end
  
  %set(gcf,'renderer','opengl');
  
  save_figure('transition_matrix', [{'Sleep_CAP'} {'Stage_Transitions'}])
  
  figure
  stage_transitions__(find(stage_transitions__ == 0)) = 1.01;
  
  imagesc((stage_transitions__))
  
  format_figure
  
  title('Sleep stage transitions (excluding self transitions)');
  ylabel('Current stage');
  xlabel('Next stage');
  set(gca,'XTickLabel',{'W','N1','N2','N3','N4','R','M'})
  set(gca,'YTickLabel',{'W','N1','N2','N3','N4','R','M'})
  
  colormap([flipud(autumn); 1 1 1])
  
  for i = 1:7
    for j = 1:7
      if stage_transitions__(j, i) <= 1
        text(i, j, num2str(round(stage_transitions__(j, i) * 10000)/10000, 2), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FontSize',16)
      end
    end
  end
  
  %set(gcf,'renderer','opengl');
  
  save_figure('transition_matrix_ignore_diag', [{'Sleep_CAP'} {'Stage_Transitions'}])

  cd(currentdir);

end