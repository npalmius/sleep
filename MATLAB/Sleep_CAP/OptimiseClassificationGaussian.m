function [ ] = OptimiseClassification(tfr_sum_all, hyp)
  % OptimiseClassification A Function to perform classification on
  %                        a tfr representation of an EEG signal.
  %	[ ] = OptimiseClassification(tfr_sum_all, hyp) 

  % Copyright (c) 2014, Nick Palmius (University of Oxford)
  % All rights reserved.
  %
  % Redistribution and use in source and binary forms, with or without
  % modification, are permitted provided that the following conditions are
  % met:
  %
  % 1. Redistributions of source code must retain the above copyright
  %    notice, this list of conditions and the following disclaimer.
  %
  % 2. Redistributions in binary form must reproduce the above copyright
  %    notice, this list of conditions and the following disclaimer in the
  %    documentation and/or other materials provided with the distribution.
  %
  % 3. Neither the name of the University of Oxford nor the names of its
  %    contributors may be used to endorse or promote products derived
  %    from this software without specific prior written permission.
  %
  % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  % "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  % LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  % A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  % HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  % SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  % LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  % DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  % THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  % (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  % OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  %	Contact: npalmius@googlemail.com
  %	Originally written by Nick Palmius, 15-Mar-2014
  
  tfr_sum_all_f = 50/2*linspace(0,1,size(tfr_sum_all, 2)/2+1);
  tfr_sum_all_t = (0:(size(tfr_sum_all, 1) - 1)) / 2 / 60;
  
  tfr_sum_sub = tfr_sum_all(:,1:length(tfr_sum_all_f));
  tfr_sum_sub_f = tfr_sum_all_f;%(1:length(tfr_sum_all_f));
  tfr_sum_F = zeros(size(tfr_sum_sub));
  
  num = size(tfr_sum_sub, 1);
  
  FEATURES = 11;
  
  F = zeros(num, FEATURES);
  
  class_idx = cell(8,1);
  
  for i = 1:num
    iMin = max(1, i - 2);
    iMax = min(num, i + 2);
    [F(i, 1)  tfr_sum_F(i, 1:15)]    = split2( mean(mean(tfr_sum_sub(iMin:iMax, 1:15)))    );
    [F(i, 2)  tfr_sum_F(i, 16:30)]   = split2( mean(mean(tfr_sum_sub(iMin:iMax, 16:30)))   );
    [F(i, 3)  tfr_sum_F(i, 31:45)]   = split2( mean(mean(tfr_sum_sub(iMin:iMax, 31:45)))   );
    [F(i, 4)  tfr_sum_F(i, 46:60)]   = split2( mean(mean(tfr_sum_sub(iMin:iMax, 46:60)))   );
    [F(i, 5)  tfr_sum_F(i, 61:75)]   = split2( mean(mean(tfr_sum_sub(iMin:iMax, 61:75)))   );
    [F(i, 6)  tfr_sum_F(i, 76:90)]   = split2( mean(mean(tfr_sum_sub(iMin:iMax, 76:90)))   );
    [F(i, 7)  tfr_sum_F(i, 91:105)]  = split2( mean(mean(tfr_sum_sub(iMin:iMax, 91:105)))  );
    [F(i, 8)  tfr_sum_F(i, 106:120)] = split2( mean(mean(tfr_sum_sub(iMin:iMax, 106:120))) );
    [F(i, 9)  tfr_sum_F(i, 121:240)] = split2( mean(mean(tfr_sum_sub(iMin:iMax, 121:240))) );
    [F(i, 10) tfr_sum_F(i, 241:390)] = split2( mean(mean(tfr_sum_sub(iMin:iMax, 241:390))) );
    [F(i, 11) tfr_sum_F(i, 391:end)] = split2( mean(mean(tfr_sum_sub(iMin:iMax, 391:end))) );
    
    class_idx{hyp(i) + 1} = [class_idx{hyp(i) + 1} i];
  end
  
  observation = load('~/Research_sync/Data/Sleep_CAP/Observation_Distributions/observations.mat');
  
  TRAIN_NO = 20;
  
  TEST_NUM = 20;
  
  results = ones(TEST_NUM, 3) * NaN;
  
  for j=1:TEST_NUM
    train = zeros(6, TRAIN_NO);
    testeq = zeros(6, TRAIN_NO);
    test1 = [];
    test = [];
    
    for i=1:6
      msize = numel(class_idx{i});
      idx = randperm(msize);
      train(i,:) = class_idx{i}(idx(1:TRAIN_NO));
      %testeq(i,:) = class_idx{i}(idx(TRAIN_NO + 1:TRAIN_NO + TRAIN_NO));
      test1 = [test1 class_idx{i}(idx(TRAIN_NO + 1))];
      test = [test class_idx{i}(idx(TRAIN_NO + 2:msize))];
    end
    
    test = [test1 test(randperm(numel(test)))];
    
    %svm = svmtrain(hyp(train(:)'), F(train(:)',:));
    
    %svmpredict(hyp,F,svm);
    
    rf = classRF_train(F(train(:)',:), hyp(train(:)'));
    Y_hat = classRF_predict(F,rf);
    
    results(j, 1) = length(find(Y_hat==hyp)) / length(hyp); %number of correct classifications
    
    initialmat = zeros(8, 1);
    initialmat(1) = 1; % Always start in wake state
    
    % Generate observation matrix based on classified values
    observemat = normalise(confusionmat(Y_hat, hyp, 'order', [0 1 2 3 4 5 6 7]), 2)
    
    % Generate transition matrix as confusion matrix of current and current + 1 state.
    transmat = normalise(confusionmat(hyp, [hyp(2:end); NaN], 'order', [0 1 2 3 4 5 6 7]), 2)
    
    B = multinomial_prob(Y_hat + 1, observemat);
    [path] = viterbi_path(initialmat, transmat, B)' - 1;
    
    results(j, 2) = length(find(path==hyp)) / length(hyp); %number of correct classifications
    
%     next_state_1  = [      0, 0.8689, 0.1311,      0,      0,      0,      0 ];
%     next_state_2  = [ 0.1772,      0, 0.7848,      0,      0, 0.0380,      0 ];
%     next_state_3  = [ 0.1598, 0.0979,      0, 0.4381,      0, 0.2938, 0.0103 ];
%     next_state_4  = [ 0.0345, 0.0086, 0.4310,      0, 0.5259,      0,      0 ];
%     next_state_5  = [ 0.0656, 0.0492, 0.3279, 0.5246,      0,      0, 0.0328 ];
%     next_state_6  = [ 0.0667, 0.0167, 0.8833,      0,      0,      0, 0.0333 ];
%     next_state_7  = [      0, 0.3333, 0.5000,      0,      0, 0.1667,      0 ];
% 
%     transition    = [ next_state_1 ;
%                       next_state_2 ;
%                       next_state_3 ;
%                       next_state_4 ;
%                       next_state_5 ;
%                       next_state_6 ;
%                       next_state_7   ];
%                     
    observemat(7,:) = [];
    observemat(:,7) = [];
%     
%     transmat(7,:) = [];
%     transmat(:,7) = [];
%     
%     transmat(logical(eye(size(transmat)))) = 0;
%     
%     transmat = spdiags (sum (transmat,2), 0, size(transmat, 1), size(transmat, 1)) \ transmat;
%     
%     transmat(find(isnan(transmat))) = 0;
%     edhmm_out = edhmm_inference(Y_hat, transmat, observemat)';
%     edhmm_out(find(edhmm_out == 6)) = 7;
%     results(j, 3) = length(find(edhmm_out==hyp)) / length(hyp); %number of correct classifications
      
    transmat(7,:) = [];
    transmat(:,7) = [];
    
    transmat(logical(eye(size(transmat)))) = 0;
    
    transmat = spdiags (sum (transmat,2), 0, size(transmat, 1), size(transmat, 1)) \ transmat;
    
    transmat(find(isnan(transmat))) = 0;
    edhmm_out = edhmm_inference_gaus_obs(F, transmat, observation, Y_hat, observemat)';
    edhmm_out(find(edhmm_out == 6)) = 7;
    results(j, 3) = length(find(edhmm_out==hyp)) / length(hyp); %number of correct classifications
    
  end
  
  figure;
  
  boxplot(results, {'Random Forest' 'RF + HMM' 'RF + EDHMM'});
  
  ylabel({'Correctly classified epochs (%)'}, 'FontSize', 15);
  txt = findobj(gca,'Type','text');
  set(txt(3:end),'FontSize', 15);
  set(txt(3:end),'VerticalAlignment', 'Middle');
  %set(findobj(gca,'Type','text'),'VerticalAlignment', 'Top');
  set(gca, 'FontSize', 12)
  
  saveas(gcf, [getenv('MATLAB_FIGURES') '/Sleep_CAP/results_box_edhmm_gaus_obs.png'], 'png');
  saveas(gcf, [getenv('MATLAB_FIGURES') '/Sleep_CAP/results_box_edhmm_gaus_obs.fig'], 'fig');
  saveas(gcf, [getenv('MATLAB_FIGURES') '/Sleep_CAP/results_box_edhmm_gaus_obs.eps'], 'epsc');
  
  figure;
  
  %h_image = subplot(4, 1, 1);
  
  %plot(hyp * 10, '-r');
  
  %hold on;
  
  h_plot1 = subplot(8, 1, 1:2);
  
  stairs(tfr_sum_all_t, hyp, '-r');
  a = axis;
  a(2) = max(tfr_sum_all_t);
  a(4) = 7;
  axis(a)

  %plot(Y_hat * 10 + 60, '-r');
  
  h_plot2 = subplot(8, 1, 3:4);
  
  stairs(tfr_sum_all_t, Y_hat, '-r');
  a = axis;
  a(2) = max(tfr_sum_all_t);
  a(4) = 7;
  axis(a)
  
  %plot(path * 10 + 120, '-r');
  
  h_plot3 = subplot(8, 1, 5:6);
  
  stairs(tfr_sum_all_t, path, '-r');
  a = axis;
  a(2) = max(tfr_sum_all_t);
  a(4) = 7;
  axis(a)
  
  h_plot4 = subplot(8, 1, 7:8);
  
  stairs(tfr_sum_all_t, edhmm_out, '-r');
  a = axis;
  a(2) = max(tfr_sum_all_t);
  a(4) = 7;
  axis(a)
  
  xlabel(h_plot4, 'Annotation time (hours)', 'FontSize', 15);
  ylabel(h_plot1, {'Sleep stage'; 'True'}, 'FontSize', 15);
  ylabel(h_plot2, {'Sleep stage'; 'Random Forest'}, 'FontSize', 15);
  ylabel(h_plot3, {'Sleep stage'; 'RF + HMM'}, 'FontSize', 15);
  ylabel(h_plot4, {'Sleep stage'; 'RF + EDHMM'}, 'FontSize', 15);
  set(h_plot1,'YTick',[0 1 2 3 4 5 7]);
  set(h_plot2,'YTick',[0 1 2 3 4 5 7]);
  set(h_plot3,'YTick',[0 1 2 3 4 5 7]);
  set(h_plot4,'YTick',[0 1 2 3 4 5 7]);
  set(h_plot1,'YTickLabel',{'W','S1','S2','S3','S4','R','M'})
  set(h_plot2,'YTickLabel',{'W','S1','S2','S3','S4','R','M'})
  set(h_plot3,'YTickLabel',{'W','S1','S2','S3','S4','R','M'})
  set(h_plot4,'YTickLabel',{'W','S1','S2','S3','S4','R','M'})
  
  set(h_plot1, 'FontSize', 12)
  set(h_plot2, 'FontSize', 12)
  set(h_plot3, 'FontSize', 12)
  set(h_plot4, 'FontSize', 12)
  
  set(h_plot1,'xtick',[])
  set(h_plot2,'xtick',[])
  set(h_plot3,'xtick',[])
  
  %hold off;
  
  saveas(gcf, [getenv('MATLAB_FIGURES') '/Sleep_CAP/results_edhmm_gaus_obs.png'], 'png');
  saveas(gcf, [getenv('MATLAB_FIGURES') '/Sleep_CAP/results_edhmm_gaus_obs.fig'], 'fig');
  saveas(gcf, [getenv('MATLAB_FIGURES') '/Sleep_CAP/results_edhmm_gaus_obs.eps'], 'epsc');
  
  return
  
  f = fopen([getenv('MATLAB_FIGURES') '/Sleep_CAP/data_truth.csv'], 'w');
  fprintf(f, 'truth\n');
  for i = 1:length(hyp)
    fprintf(f, '%i\n', hyp(i));
  end
  fclose(f);
  
  f = fopen('F:\test_to_classify.csv', 'w');
  fprintf(f, 'STATE');
  for i = 1:FEATURES
    fprintf(f, ',F%i', i);
  end
  fprintf(f, '\n');
  
  for i = test
  %for i = testeq(:)'
    fprintf(f, 'S%i', hyp(i));
    for j = 1:FEATURES
      fprintf(f, ',%.10f', F(i, j));
    end
    fprintf(f, '\n');
  end
  
  fclose(f);
  
  f = fopen('F:\test_to_train.csv', 'w');
  fprintf(f, 'STATE');
  for i = 1:FEATURES
    fprintf(f, ',F%i', i);
  end
  fprintf(f, '\n');
  
  for i = test
  %for i = testeq(:)'
    fprintf(f, 'S%i', hyp(i));
    for j = 1:FEATURES
      fprintf(f, ',%.10f', F(i, j));
    end
    fprintf(f, '\n');
  end
  
  for i = train(:)'
    fprintf(f, 'S%i', hyp(i));
    for j = 1:FEATURES
      fprintf(f, ',%.10f', F(i, j));
    end
    fprintf(f, '\n');
  end
  
  fclose(f);
  
  hold off;
end

function [ a b ] = split2 (val)
  a = val;
  b = val;
end