function [ ] = AnalyseSleepStageDurationDistributions()
  % AnalyseSleepStageDurationDistributions A Function to analyse sleep
  % stage duration distributions processed and saved by 
  % AnalyseSleepStageDuration_Thanasis function.
  %	[ ] = AnalyseSleepStageDurationDistributions() 

  % Copyright (c) 2013, Nick Palmius (University of Oxford)
  % All rights reserved.
  %
  % Redistribution and use in source and binary forms, with or without
  % modification, are permitted provided that the following conditions are
  % met:
  %
  % 1. Redistributions of source code must retain the above copyright
  %    notice, this list of conditions and the following disclaimer.
  %
  % 2. Redistributions in binary form must reproduce the above copyright
  %    notice, this list of conditions and the following disclaimer in the
  %    documentation and/or other materials provided with the distribution.
  %
  % 3. Neither the name of the University of Oxford nor the names of its
  %    contributors may be used to endorse or promote products derived
  %    from this software without specific prior written permission.
  %
  % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  % "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  % LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  % A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  % HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  % SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  % LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  % DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  % THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  % (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  % OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  %	Contact: npalmius@googlemail.com
  %	Originally written by Nick Palmius, 12-Jan-2014
  
  currentdir = pwd;
  cd(fileparts(mfilename('fullpath')));

  close All;
  
  [ durations, durations_n ] = LoadSleepStageDurations;
  
  sleep_stage_names = {'Awake', 'Sleep stage 1', 'Sleep stage 2', 'Sleep stage 3', 'Sleep stage 4', 'REM', 'Movement'};
  sleep_stage_filenamenames = {'W', 'N1', 'N2', 'N3', 'N4', 'REM', 'Movement'};
  
  dist_l = zeros(7, 7);
  
  for I = 1:7
    curr_durations = durations_n{I};

    dist_names = {'Exp', 'Log Normal', 'Poisson', 'Uniform', 'Gamma', 'F', 'Chi^2'};
    
    unipdf          = @(x, lower, upper) pdf(makedist('Uniform','lower',lower,'upper',upper), x);
    unicdf          = @(x, lower, upper) cdf(makedist('Uniform','lower',min(lower, upper),'upper',max(lower, upper)), x);

    fitfun_exp      = @(x) -sum(log(expcdf  (curr_durations+0.5, x)          - expcdf  (curr_durations-0.5, x)));
    fitfun_logn     = @(x) -sum(log(logncdf (curr_durations+0.5, x(1), x(2)) - logncdf (curr_durations-0.5, x(1), x(2))));
    fitfun_poiss    = @(x) -sum(log(poisspdf(curr_durations, x)));
    fitfun_uni      = @(x) -sum(log(unicdf  (curr_durations+0.5, x(1), x(2)) - unicdf  (curr_durations-0.5, x(1), x(2))));
    fitfun_gamma    = @(x) -sum(log(gamcdf  (curr_durations+0.5, x(1), x(2)) - gamcdf  (curr_durations-0.5, x(1), x(2))));
    fitfun_f        = @(x) -sum(log(fcdf    (curr_durations+0.5, x(1), x(2)) - fcdf    (curr_durations-0.5, x(1), x(2))));
    fitfun_chi2     = @(x) -sum(log(chi2cdf (curr_durations+0.5, x)          - chi2cdf (curr_durations-0.5, x)));

    figure
    %ax1 = gca
    hold on
    [f, x] = hist(curr_durations, 1:1:150);
    bar(x,f/trapz(x,f), 1);
    format_figure
    
    a = axis;
    a(2) = ceil((max(curr_durations) + 1) / 10) * 10;
    axis(a);
    
    axis(axis)

    xlabel('Duration in sleep stage (no. 30s epochs)');
    ylabel('Probablility density');
    title(['Potential distributions - ' sleep_stage_names{I}]);
    %set(gca,'YTickLabel', '')
    
    param_exp   = fminsearch(fitfun_exp,   20);
    param_logn  = fminsearch(fitfun_logn,  [1 1]);
    param_poiss = fminsearch(fitfun_poiss, 1);
    param_uni   = fminsearch(fitfun_uni,   [0 160]);
    param_gamma = fminsearch(fitfun_gamma, [20 20]);
    param_f     = fminsearch(fitfun_f,     [1 1]);
    param_chi2  = fminsearch(fitfun_chi2,  50);

    l_exp   = -fitfun_exp   (param_exp)
    l_logn  = -fitfun_logn  (param_logn)
    l_poiss = -fitfun_poiss (param_poiss)
    l_uni   = -fitfun_uni   (param_uni)
    l_gamma = -fitfun_gamma (param_gamma)
    l_f     = -fitfun_f     (param_f)
    l_chi2  = -fitfun_chi2  (param_chi2)

    l = [l_exp, l_logn, l_poiss, l_uni, l_gamma, l_f, l_chi2];
    l = l - (min(l) * 1.2);
    dist_l(I,:) = l;
    
    x_dist_cont = 0:0.1:160;
    x_dist_desc = 0:1:150;

    y_dist_exp   = exppdf  (x_dist_cont, param_exp);
    y_dist_logn  = lognpdf (x_dist_cont, param_logn(1), param_logn(2));
    y_dist_poiss = poisspdf(x_dist_desc, param_poiss);
    y_dist_uni   = unipdf  (x_dist_cont, param_uni(1), param_uni(2));
    y_dist_gamma = gampdf  (x_dist_cont, param_gamma(1), param_gamma(2));
    y_dist_f     = fpdf    (x_dist_cont, param_f(1), param_f(2));
    y_dist_chi2  = chi2pdf (x_dist_cont, param_chi2);
    
    %ax2 = axes('Position',get(ax1,'Position'),...
    %       'YAxisLocation','right',...
    %       'Color','none',...
    %       'XColor','k','YColor','k');
    %linkaxes([ax1 ax2],'x');
    %linkaxes([ax1 ax2],'y');
    hold on
    
    plot(x_dist_cont, y_dist_exp,   'r', 'LineWidth', 3);%,'Parent',ax2)
    plot(x_dist_cont, y_dist_logn,  'g', 'LineWidth', 3);%,'Parent',ax2)
    stairs(x_dist_desc - 0.5, y_dist_poiss, 'b', 'LineWidth', 3);%,'Parent',ax2)
    stairs(x_dist_cont, y_dist_uni, 'y', 'LineWidth', 3);%,'Parent',ax2)
    plot(x_dist_cont, y_dist_gamma, 'm', 'LineWidth', 3);%,'Parent',ax2)
    plot(x_dist_cont, y_dist_f,     'c', 'LineWidth', 3);%,'Parent',ax2)
    plot(x_dist_cont, y_dist_chi2,  '--r', 'LineWidth', 3);%,'Parent',ax2)
    
    legend({'Data' dist_names{:}});
    
    save_figure(['Duration_' sleep_stage_filenamenames{I} '_Potential_Distributions' ], [{'Sleep_CAP'} {'Duration_Distributions'}]);
    
    figure
    
    bh = bar(l);
    
    format_figure;
    
    b = axis;
    b(4) = max(l);
    axis(b);
    
    xlabel('Distribution');
    ylabel('Likelihood');
    title(['Distribution likelihoods - ' sleep_stage_names{I}]);
    
    ch=get(bh,'children');
    cd_=repmat(1:numel(l),5,1);
    cd_=[cd_(:);nan];
    set(ch,'facevertexcdata',cd_);
    colormap([1 0 0; 0 1 0; 0 0 1; 1 1 0; 1 0 1; 0 1 1; 1 0 0]);
    
    for J=1:7
      text_suffix = '';
      if l(J) == max(l)
        text_suffix = ' *';
      end
      text(J, 0, [' ' dist_names{J} text_suffix], 'HorizontalAlignment', 'left', 'VerticalAlignment', 'middle', 'FontSize', 20, 'rotation', 90)
    end
    
    set(gca,'XTickLabel', '')
    set(gca,'YTickLabel', '')
    
    save_figure(['Duration_' sleep_stage_filenamenames{I} '_Distribution_Likelihoods' ], [{'Sleep_CAP'} {'Duration_Distributions'}]);
    
    figure
    
    bar(x,f/trapz(x,f), 1);
    format_figure;
    axis(a);
    axis(axis)
    %set(gca,'YTickLabel', '')
    
    xlabel('Duration in sleep stage (no. 30s epochs)');
    ylabel('Probablility');
    %title(['Maximum likelihood distribution - ' sleep_stage_names{I}]);
    
    hold on
    
    dist_min = find(l == max(l));
    
    fprintf(['*** ' sleep_stage_filenamenames{I} ' ***\n']);
    
    param_text = '';
    for J = dist_min
      switch J
        case 1
          plot(x_dist_cont, y_dist_exp, 'r', 'LineWidth', 3)
          fprintf('  Exponential: mu=%f\n', param_exp);
        case 2
          plot(x_dist_cont, y_dist_logn, 'g', 'LineWidth', 3)
          param_text = ['\newline{\mu=' num2str(param_logn(1),2) ', \sigma=' num2str(param_logn(2),2) '}'];
          fprintf('  Lognormal: mu=%f, sigma=%f\n', param_logn(1), param_logn(2));
        case 3
          stairs(x_dist_desc - 0.5, y_dist_poiss, 'b', 'LineWidth', 3)
          fprintf('  Poisson: lambda=%f\n', param_poiss);
        case 4
          plot(x_dist_cont, y_dist_uni, 'y', 'LineWidth', 3)
          fprintf('  Uniform: lower=%f, upper=%f\n', param_uni(1), param_uni(2));
        case 5
          plot(x_dist_cont, y_dist_gamma, 'm', 'LineWidth', 3)
          param_text = ['\newline{\alpha=' num2str(param_gamma(1),2) ', \beta=' num2str(param_gamma(2),2) '}'];
          fprintf('  Gamma: alpha=%f, beta=%f\n', param_gamma(1), param_gamma(2));
        case 6
          plot(x_dist_cont, y_dist_f, 'c', 'LineWidth', 3)
          fprintf('  F: V1=%f, V2=%f\n', param_f(1), param_f(2));
        case 7
          plot(x_dist_cont, y_dist_chi2, '--r', 'LineWidth', 3)
          fprintf('  Chi-square: V=%f\n', param_chi2);
      end
    end
    
    if (length(dist_min) == 1)
      legend({'Data' [dist_names{dist_min} param_text]});
    else
      legend({'Data' dist_names{dist_min}});
    end
    
    save_figure(['Duration_' sleep_stage_filenamenames{I} '_Maximum_Likelihood_Distribution' ], [{'Sleep_CAP'} {'Duration_Distributions'}]);
  end
  
  cd(currentdir);

end