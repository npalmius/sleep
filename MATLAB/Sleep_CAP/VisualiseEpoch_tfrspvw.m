function [ ] = VisualiseEpoch_tfrspvw(eeg_data, eeg_hdr, epoch, figure_title)
  % VisualiseEpoch_tfrspvw	 A script to visualise the tfr of a single EEG
  %                          epoch for publication.
  %	[ ] = VisualiseEpoch_tfrspvw(eeg_data, eeg_hdr, epoch, figure_title)

  % Copyright (c) 2014, Nick Palmius (University of Oxford)
  % All rights reserved.
  %
  % Redistribution and use in source and binary forms, with or without
  % modification, are permitted provided that the following conditions are
  % met:
  %
  % 1. Redistributions of source code must retain the above copyright
  %    notice, this list of conditions and the following disclaimer.
  %
  % 2. Redistributions in binary form must reproduce the above copyright
  %    notice, this list of conditions and the following disclaimer in the
  %    documentation and/or other materials provided with the distribution.
  %
  % 3. Neither the name of the University of Oxford nor the names of its
  %    contributors may be used to endorse or promote products derived
  %    from this software without specific prior written permission.
  %
  % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  % "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  % LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  % A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  % HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  % SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  % LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  % DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  % THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  % (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  % OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  %	Contact: npalmius@googlemail.com
  %	Originally written by Nick Palmius, 17-Mar-2014
  
  data_sr = eeg_hdr.samples / eeg_hdr.duration;
  
  epoch_idx = (((epoch - 1) * 30 * data_sr) + 1):(epoch * 30 * data_sr);
  
  epoch_t = (epoch_idx - 1) / data_sr;
  
  data_epoch = eeg_data(epoch_idx, 1);
  
  data_epoch_spline = spline(epoch_t, data_epoch);
  
  epoch_ds_t = min(epoch_t):(1/50):max(epoch_t);
  
  data_epoch_ds = ppval(data_epoch_spline, epoch_ds_t);
  
  epoch_ds_t = epoch_ds_t - min(epoch_ds_t);
  
  [data_epoch_ds_tfr, data_epoch_ds_f] = tfrspwv(data_epoch_ds');
  
  data_epoch_ds_f = 50/2*linspace(0,1,length(data_epoch_ds_f)/2+1);
  
  data_epoch_ds_tfr_scaled = (data_epoch_ds_tfr-min(data_epoch_ds_tfr(:))) ./ (max(data_epoch_ds_tfr(:)-min(data_epoch_ds_tfr(:))));
  
  data_epoch_ds_tfr = data_epoch_ds_tfr + -min(min(data_epoch_ds_tfr));
  
  data_epoch_ds_tfr_sum = sum(data_epoch_ds_tfr_scaled, 2);
  
  data_epoch_ds_tfr_sum_norm = data_epoch_ds_tfr_sum *(1 / norm(data_epoch_ds_tfr_sum));
  
  data_epoch_ds_tfr_sum_norm_to_classify = data_epoch_ds_tfr_sum_norm(1:length(data_epoch_ds_f));
  
  data_epoch_ds_tfr_sum_norm_to_classify(1:15)    = mean(data_epoch_ds_tfr_sum_norm_to_classify(1:15)    );
  data_epoch_ds_tfr_sum_norm_to_classify(16:30)   = mean(data_epoch_ds_tfr_sum_norm_to_classify(16:30)   );
  data_epoch_ds_tfr_sum_norm_to_classify(31:45)   = mean(data_epoch_ds_tfr_sum_norm_to_classify(31:45)   );
  data_epoch_ds_tfr_sum_norm_to_classify(46:60)   = mean(data_epoch_ds_tfr_sum_norm_to_classify(46:60)   );
  data_epoch_ds_tfr_sum_norm_to_classify(61:75)   = mean(data_epoch_ds_tfr_sum_norm_to_classify(61:75)   );
  data_epoch_ds_tfr_sum_norm_to_classify(76:90)   = mean(data_epoch_ds_tfr_sum_norm_to_classify(76:90)   );
  data_epoch_ds_tfr_sum_norm_to_classify(91:105)  = mean(data_epoch_ds_tfr_sum_norm_to_classify(91:105)  );
  data_epoch_ds_tfr_sum_norm_to_classify(106:120) = mean(data_epoch_ds_tfr_sum_norm_to_classify(106:120) );
  data_epoch_ds_tfr_sum_norm_to_classify(121:240) = mean(data_epoch_ds_tfr_sum_norm_to_classify(121:240) );
  data_epoch_ds_tfr_sum_norm_to_classify(241:390) = mean(data_epoch_ds_tfr_sum_norm_to_classify(241:390) );
  data_epoch_ds_tfr_sum_norm_to_classify(391:end) = mean(data_epoch_ds_tfr_sum_norm_to_classify(391:end) );
  
  figure;
  
  h_data = subplot(7, 7, 2:7);
  
  plot(epoch_ds_t, data_epoch_ds, 'LineWidth', 1)
  
  h_tfr = subplot(7, 7, [9:14, 16:21, 23:28, 30:35, 37:42, 44:49]);
  
  imagesc(epoch_ds_t, data_epoch_ds_f, data_epoch_ds_tfr(1:length(data_epoch_ds_f),:));
  
  h_tfr_sum = subplot(7, 7, [8, 15, 22, 29, 36, 43]);
  
  h = plot(data_epoch_ds_tfr_sum_norm(1:length(data_epoch_ds_f)), data_epoch_ds_f, 'LineWidth', 1);
  %plot(data_epoch_ds_tfr_sum_norm(1:length(data_epoch_ds_f)), data_epoch_ds_f, data_epoch_ds_tfr_sum_norm_to_classify + max(data_epoch_ds_tfr_sum_norm) - min(data_epoch_ds_tfr_sum_norm_to_classify), data_epoch_ds_f, 'LineWidth', 1);
  
  hold on;
  axis(axis);
  plot([0 1], [0.5 0.5], 'LineWidth', 2, 'Color', [0.8,0.8,0.8]);
  plot([0 1], [1 1],     'LineWidth', 2, 'Color', [0.8,0.8,0.8]);
  plot([0 1], [1.5 1.5], 'LineWidth', 2, 'Color', [0.8,0.8,0.8]);
  plot([0 1], [2 2],     'LineWidth', 2, 'Color', [0.8,0.8,0.8]);
  plot([0 1], [2.5 2.5], 'LineWidth', 2, 'Color', [0.8,0.8,0.8]);
  plot([0 1], [3 3],     'LineWidth', 2, 'Color', [0.8,0.8,0.8]);
  plot([0 1], [3.5 3.5], 'LineWidth', 2, 'Color', [0.8,0.8,0.8]);
  plot([0 1], [4 4],     'LineWidth', 2, 'Color', [0.8,0.8,0.8]);
  plot([0 1], [8 8],     'LineWidth', 2, 'Color', [0.8,0.8,0.8]);
  plot([0 1], [13 13],   'LineWidth', 2, 'Color', [0.8,0.8,0.8]);
  
  uistack(h(1), 'top')
  
  hold off;
  
  set(h_tfr,'YDir','normal');
  set(h_tfr_sum,'XDir','reverse');
  
  xlabel(h_tfr, 'Epoch time (seconds)', 'FontSize', 15);
  ylabel(h_tfr_sum, 'Frequency (Hz)', 'FontSize', 15);
  xlabel(h_tfr_sum, '$\displaystyle\sum_tf(t)$', 'Interpreter', 'LaTex', 'FontSize', 15);
  
  title(h_data, figure_title, 'FontSize', 20);
  
  set(h_data, 'FontSize', 12)
  set(h_tfr, 'FontSize', 12)
  set(h_tfr_sum, 'FontSize', 12)
  
  set(h_data,'xtick',[])
  set(h_tfr,'ytick',[])
  set(h_tfr_sum,'xtick',[])
  
  a = axis(h_data);
  a(3) = eeg_hdr.physicalMin;
  a(4) = eeg_hdr.physicalMax;
  axis(h_data, a);
  ylabel(h_data, 'EEG\newline(\mu V)', 'FontSize', 14);