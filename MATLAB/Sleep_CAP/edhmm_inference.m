function [ out ] = edhmm_inference(data, transition, observation)
  % edhmm_inference A Function to perform edhmm inference.
  %	[ out ] = edhmm_inference(data, transition, observation)

  % Copyright (c) 2014, Frank Wood and Nick Palmius (University of Oxford)
  % All rights reserved.
  %
  % Redistribution and use in source and binary forms, with or without
  % modification, are permitted provided that the following conditions are
  % met:
  %
  % 1. Redistributions of source code must retain the above copyright
  %    notice, this list of conditions and the following disclaimer.
  %
  % 2. Redistributions in binary form must reproduce the above copyright
  %    notice, this list of conditions and the following disclaimer in the
  %    documentation and/or other materials provided with the distribution.
  %
  % 3. Neither the name of the University of Oxford nor the names of its
  %    contributors may be used to endorse or promote products derived
  %    from this software without specific prior written permission.
  %
  % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  % "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  % LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  % A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  % HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  % SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  % LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  % DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  % THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  % (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  % OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  %	Contact: npalmius@googlemail.com
  %	Originally written by Frank Wood, 7-Apr-2014
  
  %% setup

  % control the random number generator
  %rng(3); 

  % number of states
  K = 7;

  % data
  %y = randsample(1:3,120,'true');
  y = data+1;

  % initial duration and state (the state doesn't matter if d_0 == 1)
  d_0 = 1; 
  z_0 = 1;

  % num sweeps
  num_sweeps = 25;

  % number of time steps
  T = length(y);

  % datastructure indices
  STA = 1;
  DUR = 2;

  % datastructure for forward backward
  alpha = cell(T,1);

  % slice variables
  u = ones(T,1)*.01;

  % current state
  z = zeros(T,2);


  %% transition distributions
  % A = rand(K,K);
  % A = A.*(1-eye(K));
  % for k = 1:K
  %     A(k,:) = A(k,:)./sum(A(k,:));
  % end

  transition = transition*100 + ones(size(transition));

  for k = 1:7
    transition(k,:) = transition(k,:)./sum(transition(k,:));
  end

  transition(find(isnan(transition))) = 0;

  A = transition;


  %% duration distributions
  
  S = zeros(K,4*T);

  %% pre-compute duration distributions and durations above the slice 
  % this is wildly inefficient and stupid but we're in a hurry
  for t = 1:4*T
      S(1,t) = logncdf(t, 2.000776, 1.358540)-logncdf(t-1, 2.000776, 1.358540);
      S(2,t) = logncdf(t, 0.883984,0.715914)-logncdf(t-1, 0.883984,0.715914);
      S(3,t) = gamcdf(t, 1.027963,23.790777)-gamcdf(t-1,  1.027963,23.790777);
      S(4,t) = gamcdf(t, 2.041554,4.437275)-gamcdf(t-1, 2.041554,4.437275);
      S(5,t) = gamcdf(t, 1.146168,26.816168)-gamcdf(t-1, 1.146168,26.816168);
      S(6,t) = gamcdf(t, 1.562251,28.143756)-gamcdf(t-1, 1.562251,28.143756);
      S(7,t) = gamcdf(t, 259.586246,0.003404)-gamcdf(t-1, 259.586246,0.003404);
  end

  plot(S')

  % Distributions
  %
  % state 1:  lognormal: mu = 2.000776, sigma = 1.358540
  % state 2:  lognormal: mu = 0.883984, sigma = 0.715914
  % state 3:  gamma: alpha = 1.027963,   beta = 23.790777
  % state 4:  gamma: alpha = 2.041554,   beta = 4.437275
  % state 5:  gamma: alpha = 1.146168,   beta = 26.816168
  % state 6:  gamma: alpha = 1.562251,   beta = 28.143756
  % state 7:  gamma: alpha = 259.586246, beta = 0.003404



  %% observations distributions
  % O = 5*eye(K,K)+ones(K,K);
  % for k = 1:K
  %     O(k,:) = O(k,:)./sum(O(k,:));
  % end

  observation = observation*100 + ones(size(observation));

  for k = 1:7
    observation(k,:) = observation(k,:) ./ sum(observation(k,:));
  end

  observation(find(isnan(observation))) = 0;

  O = observation;


  %% sampler
  z_record = zeros(num_sweeps,T,2);
  log_probability_record = zeros(num_sweeps,1);

  for sweep = 1:num_sweeps


      % compute alpha sweep first time point
      t=1;
      if d_0 == 1
          % add to set of z(1)'s all states at all durations over the slice
          z_1 = [];
          for k = 1:K
              % compute the observation likelihood for this state
              ooo = O(k,y(t));

              % durations above the slice
              durs_over_slice = find(S(k,:)>u(1) == 1);
              for dind = 1:length(durs_over_slice)
                  if  ooo > 0
                      z_1 = [z_1 ; [k durs_over_slice(dind) ooo]];
                  end
              end
          end
          z_1(:,3) = z_1(:,3)./sum(z_1(:,3));
          alpha{1} = z_1;
      else
          alpha{1} = [z_0 d_0-1 1];
      end

      % compute alpha sweep remaining time points
      for t = 2:T
          alpha_t_minus_1 = alpha{t-1};
          alpha{t} = [];
          for ind = 1:size(alpha_t_minus_1,1)
              if alpha_t_minus_1(ind,DUR) == 1 % if the duration has counted down to one
                  for k = 1:K % try all other subsequent states
                      durs_over_slice = find(A(alpha_t_minus_1(ind,STA),k)*S(k,:)>u(t)); % find all transitions above the slice
                      for dind = 1:length(durs_over_slice) % for those durations
                          ooo = O(k,y(t))*alpha_t_minus_1(ind,3); % compute and check the likelihood too
                          if ooo > 0 
                              i = []; % check to see if alpha{t} already has that state and duration in it
                              if size(alpha{t},1)>0
                                  i = find(alpha{t}(:,1) == k & alpha{t}(:,2) == durs_over_slice(dind));
                              end
                              if ~isempty(i)
                                  alpha{t}(i,3) = alpha{t}(i,3)+ooo;
                              else
                                  alpha{t} = [alpha{t} ; [k durs_over_slice(dind) ooo]];
                              end
                          end
                      end
                  end
              else % check to see if alpha{t} already has that state and duration in it
                  i = [];
                  if size(alpha{t})>0
                      i = find(alpha{t}(:,1) == alpha_t_minus_1(ind,1) & alpha{t}(:,2) ==  alpha_t_minus_1(ind,2)-1);
                  end
                  if ~isempty(i);
                      alpha{t}(i,3) = alpha{t}(i,3)+O(alpha_t_minus_1(ind,1),y(t))*alpha_t_minus_1(ind,3);
                  else
                      alpha{t} = [alpha{t} ; [alpha_t_minus_1(ind,1) alpha_t_minus_1(ind,2)-1 O(alpha_t_minus_1(ind,1),y(t))*alpha_t_minus_1(ind,3)]];
                  end
              end
          end
          % normalize alpha
          f = alpha{t};
          f(:,3) = f(:,3)./sum(f(:,3));
          alpha{t} = f;
      end


      % sample z backwards
      z_t = randsample(1:length(alpha{T}(:,3)),1,true,alpha{T}(:,3));
      z_record(sweep,T,STA) = alpha{T}(z_t,1);
      z_record(sweep,T,DUR) = alpha{T}(z_t,2);
      for t = T-1:-1:1
          z_t_minus_1 = [];

          % find out if the same state with duration greater by one is in the
          % preceding state set and add it to the candidates if so
          i = find(alpha{t}(:,1) == z_record(sweep,t+1,STA) & alpha{t}(:,2)-1 == z_record(sweep,t+1,DUR));
          if ~isempty(i)
              % this can be added because the count-down will always pass the
              % slice
              z_t_minus_1 = [z_t_minus_1; alpha{t}(i,:)];
          end

          % find the states that must transition and check to see if they
          % are over the slice
          zp = [];
          must_trans_inds = find(alpha{t}(:,2) == 1); 
          if ~isempty(must_trans_inds)

              for i = 1:length(must_trans_inds)
                  pz = alpha{t}(must_trans_inds(i),1);
                  pd = alpha{t}(must_trans_inds(i),2);
                  if S(z_record(sweep,t+1,STA),z_record(sweep,t+1,DUR))*A(pz,z_record(sweep,t+1,STA)) > u(t+1)
                      zp = [zp; alpha{t}(must_trans_inds(i),:)];
                  end
              end
          end
          z_t_minus_1 = [z_t_minus_1; zp];

          if size(z_t_minus_1,1) == 0
              disp('foo');
          end

          z_t_minus_1(:,3) = z_t_minus_1(:,3)./sum(z_t_minus_1(:,3));

          ind = randsample(1:length(z_t_minus_1(:,3)),1,true,z_t_minus_1(:,3));

          z_record(sweep,t,STA) = z_t_minus_1(ind,1);
          z_record(sweep,t,DUR) = z_t_minus_1(ind,2);
      end

      % sample u_t's 

      u(1) = rand()*S(z_record(sweep,1,STA),z_record(sweep,1,DUR));

      for t=1:T-1
          if z_record(sweep,t,STA) == z_record(sweep,t+1,STA)
              u(t+1) = rand();
          else
         u(t+1) = rand()*A(z_record(sweep,t,STA),z_record(sweep,t+1,STA))*...
                       S(z_record(sweep,t+1,STA),z_record(sweep,t+1,DUR));
          end
      end


      % and compute log joint


      lj = log(S(z_record(sweep,1,STA),z_record(sweep,1,DUR)));
      for t=1:T-1
          if(z_record(sweep,t,STA)~=z_record(sweep,t+1,STA))
          lj = lj+ log(A(z_record(sweep,t,STA),z_record(sweep,t+1,STA))) + ...
                       log(S(z_record(sweep,t,STA),z_record(sweep,t,DUR))) + ...
                       log(O(z_record(sweep,t,STA),y(t)));
          else
              lj = lj + log(O(z_record(sweep,t,STA),y(t)));
          end
      end
      lj = lj + log(S(z_record(sweep,t,STA),z_record(sweep,t,DUR))) + ...
                       log(O(z_record(sweep,t,STA),y(T)));
      log_probability_record(sweep) = lj;

      figure(1)
      plot(1:sweep,log_probability_record(1:sweep));
      set(gca,'XLim',[.1 num_sweeps]);

      drawnow

  end

  %sum(data_truth' == z_record(sweep,:,STA)-1)/T

  out = z_record(max(find(log_probability_record == max(log_probability_record))),:,STA)-1;

  %    figure(2)
  %    stairs(data_truth,'r')
  %    hold on
  %    stairs(z_record(sweep,:,STA)-1,'b');
  %    hold off

  % pick the z_record with the highest log joint
