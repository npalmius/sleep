function [ ] = AnalyseAllEEG_tfrspvw()
  % AnalyseAllEEG_tfrspvw	 A script to perform spwvd transform on all
  %                        EEG data.
  %	[ ] = AnalyseAllEEG_tfrspvw()

  % Copyright (c) 2014, Nick Palmius (University of Oxford)
  % All rights reserved.
  %
  % Redistribution and use in source and binary forms, with or without
  % modification, are permitted provided that the following conditions are
  % met:
  %
  % 1. Redistributions of source code must retain the above copyright
  %    notice, this list of conditions and the following disclaimer.
  %
  % 2. Redistributions in binary form must reproduce the above copyright
  %    notice, this list of conditions and the following disclaimer in the
  %    documentation and/or other materials provided with the distribution.
  %
  % 3. Neither the name of the University of Oxford nor the names of its
  %    contributors may be used to endorse or promote products derived
  %    from this software without specific prior written permission.
  %
  % THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  % "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  % LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  % A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  % HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  % SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  % LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  % DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  % THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  % (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  % OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  %	Contact: npalmius@googlemail.com
  %	Originally written by Nick Palmius, 14-Mar-2014
  
  %fprintf('Processing n1 C4A1...\n');
  %AnalyseEEG_tfrspvw('n1', 'C4A1')
  
  %fprintf('Processing n2 C4A1...\n');
  %AnalyseEEG_tfrspvw('n2', 'C4A1')
  
  %fprintf('Processing n3 C4A1...\n');
  %AnalyseEEG_tfrspvw('n3', 'C4A1')
  
  %fprintf('Processing n4 C4A1...\n');
  %AnalyseEEG_tfrspvw('n4', 'C4A1')
  
  %fprintf('Processing n5 C4A1...\n');
  %AnalyseEEG_tfrspvw('n5', 'C4A1')
  
  %fprintf('Processing n6 C3A2...\n');
  %AnalyseEEG_tfrspvw('n6', 'C3A2')
  
  %fprintf('Processing n7 C3A2...\n');
  %AnalyseEEG_tfrspvw('n7', 'C3A2')
  
  %fprintf('Processing n8 C3A2...\n');
  %AnalyseEEG_tfrspvw('n8', 'C3A2')
  
  %fprintf('Processing n9 C3A2...\n');
  %AnalyseEEG_tfrspvw('n9', 'C3A2')
  
  %fprintf('Processing n10 C4A1...\n');
  %AnalyseEEG_tfrspvw('n10', 'C4A1')
  
  %fprintf('Processing n11 C4A1...\n');
  %AnalyseEEG_tfrspvw('n11', 'C4A1')
  
  %fprintf('Processing n12 C4A1...\n');
  %AnalyseEEG_tfrspvw('n12', 'C4A1')
  
  %fprintf('Processing n13 C4A1...\n');
  %AnalyseEEG_tfrspvw('n13', 'C4A1')
  
  %fprintf('Processing n14 C4A1...\n');
  %AnalyseEEG_tfrspvw('n14', 'C4A1')
  
  %fprintf('Processing n15 C4A1...\n');
  %AnalyseEEG_tfrspvw('n15', 'C4A1')
  
  %fprintf('Processing n16 C4A1...\n');
  %AnalyseEEG_tfrspvw('n16', 'C4A1')
  
end