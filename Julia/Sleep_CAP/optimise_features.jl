# Copyright (c) 2014, Nick Palmius (University of Oxford)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the University of Oxford nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Contact: npalmius@googlemail.com
# Originally written by Nick Palmius, 22-Sep-2014

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# THIS IS UNTESTED / UNFINISHED / NOT WORKING
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

cd("C:\\Research\\git\\Sleep\\Julia\\Sleep_CAP")

push!(LOAD_PATH, "..\\..\\..\\edhmm-julia");

using MAT
using Distributions
using EDHMM
using Plotly

pop!(LOAD_PATH);

include("..\\..\\..\\plotly-julia\\plotly_signin.jl");

FEATURES = 11;
MAX_FEATURES = 11;
CLASSES = 7; # {0:5, 7}

HYP_PATH = "C:\\Research\\Sync\\Data\\Sleep_CAP\\";
TFR_PATH = string(HYP_PATH, "tfr\\");

##test_file = "n1";

hyp_files = filter!(r"^n[0-9]+\.hyp\.mat$", readdir(HYP_PATH))
tfr_files = filter!(r"^n[0-9]+\.C[1-4]A[1-4]\.all\.mat$", readdir(TFR_PATH))

valid_files = nothing;

for f = hyp_files
    file_id = match(r"(n[0-9]+)", f).match;
    tfr_count = length(filter(Regex(string("^", file_id, "\\.")), tfr_files));

    @assert (tfr_count == 1) string(tfr_count, " tfr files found corresponding to hyp file ", f, ".")

    if valid_files == nothing
        valid_files = [file_id];
    else
        valid_files = [valid_files, file_id];
    end
end

for f = tfr_files
    file_id = match(r"(n[0-9]+)", f).match;
    hyp_count = length(filter(Regex(string("^", file_id, "\$")), valid_files));

    @assert (hyp_count == 1) string(hyp_count, " hyp files found corresponding to tfr file ", f, ".")
end

data_hyp_total = cell(length(valid_files), 1);
data_raw_total = cell(length(valid_files), 1);
data_valid = zeros(length(valid_files), 1);

i = 0;

for file_id = valid_files
    print(string("Loading ", file_id, ": "));

    i += 1;

    hyp_file = filter(Regex(string("^", file_id, "\\.")), hyp_files)
    tfr_file = filter(Regex(string("^", file_id, "\\.")), tfr_files)

    @assert (length(hyp_file) == 1 && length(tfr_file) == 1) string("Error loading ", file_id, " files.")

    file = matopen(string(HYP_PATH, hyp_file[1]))
    names(file)
    this_hyp = read(file, "hyp");
    close(file)

    file = matopen(string(TFR_PATH, tfr_file[1]))
    this_tfr = read(file, "tfr_sum_all");
    close(file)

    this_hyp = min(this_hyp[:,1], CLASSES - 1); # Change 7's to 6's
    this_hyp = this_hyp + 1; # Make 1-based for easy processing

    this_tfr_f = 50/2*linspace(0,1,int64(size(this_tfr, 2)/2+1));
    this_tfr_half = this_tfr[:,1:length(this_tfr_f)];

    tfr_len = size(this_tfr_half, 1);

    if tfr_len != length(this_hyp)
        println(string("Skipped: length mismatch - hyp length: ", length(this_hyp), ", data length: ", tfr_len))
        data_hyp_total[i] = nothing;
        data_raw_total[i] = nothing;
        continue;
    end

    data_hyp_total[i] = this_hyp;
    data_raw_total[i] = this_tfr_half;
    data_valid[i] = 1;

    println("Done.");
end

x = [15, 30, 45, 60, 75, 90, 105, 120, 240, 390]

hyp_total = nothing;
features_total = nothing;

function get_fitness(x::Vector)
    hyp_total = nothing;
    features_total = nothing;

    for file = find(data_valid)
        tfr_len = size(data_raw_total[file], 1);
        f_len = size(data_raw_total[file], 2);

        f_start = int64(ones(FEATURES, 1));
        f_end   = int64(ones(FEATURES, 1)) * f_len;
        f_start[2:FEATURES] = int64(round(x)) + 1;
        f_end[1:(FEATURES-1)] = int64(round(x));

        F = zeros(tfr_len, FEATURES);

        for t = 1:tfr_len
            iMin = max(1, t - 2);
            iMax = min(tfr_len, t + 2);

            for f = 1:FEATURES
                F[t,  f] = mean(mean(data_raw_total[file][iMin:iMax, f_start[f]:f_end[f] ]));
            end
        end

        if hyp_total == nothing
            hyp_total = data_hyp_total[file];
            features_total = F;
        else
            hyp_total = [hyp_total; data_hyp_total[file]];
            features_total = [features_total; F];
        end
    end

    f_means = ones(FEATURES, CLASSES) * NaN;
    f_stds = ones(FEATURES, CLASSES) * NaN;

    for f = 1:FEATURES
        for c = 1:CLASSES
            idx = findin(hyp_total, c);

            if length(idx) > 0
                f_means[f, c] = mean(features_total[idx, f]);
                f_stds[f, c] = std(features_total[idx, f]);
            end
        end
    end

    f_diffs = zeros(FEATURES, CLASSES);

    for f = 1:FEATURES
        for c1 = 1:CLASSES
            for c2 = 1:CLASSES
                if c1 != c2 && !isnan(f_means[f, c1]) && !isnan(f_means[f, c2])
                    f_diffs[f, c1] += abs(f_means[f, c1] - f_means[f, c2]);
                end
            end
        end
    end

    return sum(sum(f_diffs))
end

x_max_fitness = 0;
x_max = nothing;

f_max = size(data_raw_total[find(data_valid)[1]], 2);

FEATURES = 2;

this_max = 0;

fitness_data = ones(MAX_FEATURES, f_max) * NaN;

println(f_max);

for i = 1:int64(round((f_max / 2)))
    #if i == 1
    #    x2 = [15, 30, 45, 60, 75, 90, 105, 120, 240, 390]
    #else
    #    x2 = sort(int64(round(rand(Uniform(2, f_max), FEATURES - 1))))
    #end

    x2 = [i]

    print(string(i, " : "));

    f_fit = get_fitness(x2)
    print(f_fit)

    fitness_data[1, i] = f_fit;

    if f_fit > x_max_fitness
        x_max_fitness = f_fit
        this_max = i;
        print(" * ")
        println(x2)
    else
        println("")
    end
end

x_max = [this_max];

    for file = find(data_valid)
        tfr_len = size(data_raw_total[file], 1);
        f_len = size(data_raw_total[file], 2);

        f_start = int64(ones(FEATURES, 1));
        f_end   = int64(ones(FEATURES, 1)) * f_len;
        f_start[2:FEATURES] = int64(round(x_max)) + 1;
        f_end[1:(FEATURES-1)] = int64(round(x_max));

        F = zeros(tfr_len, FEATURES);

        for t = 1:tfr_len
            iMin = max(1, t - 2);
            iMax = min(tfr_len, t + 2);

            for f = 1:FEATURES
                F[t,  f] = mean(mean(data_raw_total[file][iMin:iMax, f_start[f]:f_end[f] ]));
            end
        end

        if hyp_total == nothing
            hyp_total = data_hyp_total[file];
            features_total = F;
        else
            hyp_total = [hyp_total; data_hyp_total[file]];
            features_total = [features_total; F];
        end
    end

for f = 1:FEATURES
    feature_plot_data = nothing

    for i = 1:CLASSES
        idx = findin(hyp_total, i);

        if length(idx) > 0
            trace = [
              "x" => features_total[idx, f]', 
              "opacity" => 0.5, 
              "type" => "histogram",
              "name" => string("class ", i)
            ]

            if feature_plot_data == nothing
                feature_plot_data = [trace];
            else
                feature_plot_data = [feature_plot_data, trace];
            end
        end
    end

    layout = ["barmode" => "overlay"]

    response = Plotly.plot([feature_plot_data], ["layout" => layout, "filename" => string("sleep-data/feature-", f), "fileopt" => "overwrite"])
    print(string("Plotting feature ", f, " raw data : "));
    plot_url = response["url"];
    println(plot_url);
end

    sampler_log_liklihood = [
        "x" => [1:f_max],
        "y" => fitness_data[1, :],
        "mode" => "lines",
        "line" => ["shape" => "hvh"],
        "type" => "scatter",
        "showlegend" => false
    ];

    response = Plotly.plot([sampler_log_liklihood], ["filename" => "sleep-data/feature-optimisation", "fileopt" => "overwrite"]);
    plot_url = response["url"];
    print("Plotting optimisation per split : ");
    println(plot_url);
