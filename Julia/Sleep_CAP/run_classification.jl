# Copyright (c) 2014, Nick Palmius (University of Oxford)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the University of Oxford nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Contact: npalmius@googlemail.com
# Originally written by Nick Palmius, 22-Sep-2014

cd("C:\\Research\\git\\Sleep\\Julia\\Sleep_CAP")

push!(LOAD_PATH, "..\\..\\..\\edhmm-julia");

using MAT
using Distributions
using EDHMM
using Plotly
using RDatasets
using DecisionTree
using MLBase

pop!(LOAD_PATH);

include("..\\..\\..\\plotly-julia\\plotly_signin.jl");

include("..\\..\\..\\edhmm-julia\\edhmm_beamsampler.jl")

#x = [15, 30, 45, 60, 75, 90, 105, 120, 240, 390]
#x = [15, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 240, 300, 390];
 x = [15, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 390];

FEATURES = length(x) + 1;
CLASSES = 7; # {0:5, 7}

HYP_PATH = "C:\\Research\\Sync\\Data\\Sleep_CAP\\";
TFR_PATH = string(HYP_PATH, "tfr\\");
RESULTS_PATH = string(HYP_PATH, "results\\");

#test_files = [ "n1", "n2", "n3", "n4", "n5", "n6", "n7", "n8", "n10", "n11", "n12", "n13", "n14", "n15", "n16" ] # n9 crashes the edhmm.
test_files = [ "n16" ]

hyp_files = filter!(r"^n[0-9]+\.hyp\.mat$", readdir(HYP_PATH))
tfr_files = filter!(r"^n[0-9]+\.C[1-4]A[1-4]\.all\.mat$", readdir(TFR_PATH))

valid_files = nothing;

for f = hyp_files
    file_id = match(r"(n[0-9]+)", f).match;
    tfr_count = length(filter(Regex(string("^", file_id, "\\.")), tfr_files));

    @assert (tfr_count == 1) string(tfr_count, " tfr files found corresponding to hyp file ", f, ".")

    if valid_files == nothing
        valid_files = [file_id];
    else
        valid_files = [valid_files, file_id];
    end
end

for f = tfr_files
    file_id = match(r"(n[0-9]+)", f).match;
    hyp_count = length(filter(Regex(string("^", file_id, "\$")), valid_files));

    @assert (hyp_count == 1) string(hyp_count, " hyp files found corresponding to tfr file ", f, ".")
end

for test_file = test_files
    println("===============================================================================");
    println(string("Processing subject ", test_file));

    data = EDHMM_Data();

    initial_state = zeros(CLASSES, 1)
    transitions = zeros(CLASSES, CLASSES)
    durations = cell(CLASSES, 1)

    for i = 1:CLASSES
        durations[i] = zeros(1, 0);
    end

    hyp_total = nothing;
    features_total = nothing;

    for file_id = valid_files
        print(string("Loading ", file_id, ": "));

        hyp_file = filter(Regex(string("^", file_id, "\\.")), hyp_files)
        tfr_file = filter(Regex(string("^", file_id, "\\.")), tfr_files)

        @assert (length(hyp_file) == 1 && length(tfr_file) == 1) string("Error loading ", file_id, " files.")

        file = matopen(string(HYP_PATH, hyp_file[1]))
        names(file)
        this_hyp = read(file, "hyp");
        close(file)

        file = matopen(string(TFR_PATH, tfr_file[1]))
        this_tfr = read(file, "tfr_sum_all");
        close(file)

        this_hyp = min(this_hyp[:,1], CLASSES - 1); # Change 7's to 6's
        this_hyp = this_hyp + 1; # Make 1-based for easy processing

        this_tfr_f = 50/2*linspace(0,1,int64(size(this_tfr, 2)/2+1));
        this_tfr_half = this_tfr[:,1:length(this_tfr_f)];

        tfr_len = size(this_tfr_half, 1);
        f_len = size(this_tfr_half, 2);

        F = zeros(tfr_len, FEATURES);

        if length(this_hyp) > tfr_len
            this_hyp = this_hyp[1:tfr_len];
        end

        if tfr_len != length(this_hyp)
            println(string("Skipped: length mismatch - hyp length: ", length(this_hyp), ", data length: ", tfr_len))
            continue;
        end

        last_change = 1;

        f_start = int64(ones(FEATURES, 1));
        f_end   = int64(ones(FEATURES, 1)) * f_len;

        f_start[2:FEATURES] = int64(round(x)) - 1;
        f_end[1:(FEATURES-1)] = int64(round(x)) + 2;

        for t = 1:tfr_len
            iMin = max(1, t - 2);
            iMax = min(tfr_len, t + 2);

            for f = 1:FEATURES
                F[t,  f] = mean(mean(this_tfr_half[iMin:iMax, f_start[f]:f_end[f] ]));
            end

            if file_id != test_file
                f_this = this_hyp[t];

                if t == 1
                    initial_state[f_this] += 1;
                else
                    f_prev = this_hyp[t - 1];

                    if f_this != f_prev
                        transitions[f_prev, f_this] += 1;

                        durations[f_prev] = [durations[f_prev] (t - last_change)]
                        last_change = t;
                    end

                    if t == tfr_len
                        durations[f_prev] = [durations[f_prev] (t - last_change + 1)]
                    end
                end
            end
        end

        if file_id == test_file
            data.x = [1:tfr_len];
            data.y = F';
            data.z = this_hyp;

            println("Done (test file).");
        else
            if hyp_total == nothing
                hyp_total = this_hyp;
                features_total = F;
            else
                hyp_total = [hyp_total; this_hyp];
                features_total = [features_total; F];
            end

            println("Done.");
        end
    end

#    for f = 1:FEATURES
#        feature_plot_data = nothing
#
#        x0 = randn(500)
#
#        for i = 1:CLASSES
#            idx = findin(hyp_total, i);
#
#            if length(idx) > 0
#                trace = [
#                  "x" => features_total[idx, f]', 
#                  "opacity" => 0.5, 
#                  "type" => "histogram",
#                  "name" => string("class ", i)
#                ]
#
#                if feature_plot_data == nothing
#                    feature_plot_data = [trace];
#                else
#                    feature_plot_data = [feature_plot_data, trace];
#                end
#            end
#        end
#
#        layout = ["barmode" => "overlay"]
#
#        #response = Plotly.plot([feature_plot_data], ["layout" => layout, "filename" => string("sleep-data/feature-", f), "fileopt" => "overwrite"])
#        #print(string("Plotting feature ", f, " raw data : "));
#        #plot_url = response["url"];
#        #println(plot_url);
#    end

    θ = EDHMM_Params();

    θ.π = Categorical(vec(initial_state / sum(initial_state)));
    θ.a = cell(CLASSES, 1);
    θ.o = cell(CLASSES, 1);
    θ.d = cell(CLASSES, 1);

    for i = 1:CLASSES
        idx = findin(hyp_total, i);

        if length(idx) > FEATURES
            θ.o[i] = fit(MvNormal, features_total[idx, :]')
            if i == 1 || i == 2
                θ.d[i] = fit(LogNormal, durations[i]);
            else
                θ.d[i] = fit(Gamma, durations[i]);
            end
        else
            transitions[:, i] = 0; # We don't have enough data to calculate observation distribution so ignore.
            θ.a[i] = 0;
            θ.o[i] = 0;
            θ.d[i] = 0;
        end
    end

    for i = 1:CLASSES
        if (sum(transitions[i, :]) > 0)
            θ.a[i] = Categorical(vec(transitions[i, :] / sum(transitions[i, :])));
        else
            θ.a[i] = 0;
        end
    end

    data.θ = θ;

    result, edhmm_log_lik, edhmm_u, edhmm_z, edhmm_l = edhmm_beam_sample(data)

    #confusmat(CLASSES, int64(data.z), int64(result))

    forest = build_forest(int64(hyp_total), features_total, 3, 30);

    predictions = apply_forest(forest, data.y');

    println(string("RF percent correct: ", correctrate(vec(int64(data.z)), vec(int64(predictions)))));

    predicted_state = [
        "x" => data.x,
        "y" => predictions,
        "mode" => "lines",
        "line" => ["shape" => "hvh"],
        "type" => "scatter",
        "name" => "Inferred state"
    ];

    true_state = [
        "x" => data.x,
        "y" => data.z + CLASSES + 1,
        "mode" => "lines",
        "line" => ["shape" => "hvh"],
        "type" => "scatter",
        "name" => "True state"
    ];

    result_plot_data = [true_state, predicted_state];

    response = try
        Plotly.plot([result_plot_data], ["filename" => "edhmm-rf-result", "fileopt" => "overwrite"]);
    catch
        {"url" => "Error sending plot."};
    end
    print("Plotting random forest result : ");
    plot_url = response["url"];
    println(plot_url);

    file = matopen(string(RESULTS_PATH, test_file, ".mat"), "w");
    write(file, "hyp", int64(data.z))
    write(file, "result_edhmm", result)
    write(file, "result_rf", predictions)
    write(file, "sampler_likelihood", edhmm_log_lik)
    write(file, "sampler_u", edhmm_u)
    write(file, "sampler_z", edhmm_z)
    write(file, "sampler_l", edhmm_l)
    write(file, "accuracy_edhmm", correctrate(vec(int64(data.z)), vec(int64(result))))
    write(file, "accuracy_rf", correctrate(vec(int64(data.z)), vec(int64(predictions))))
    write(file, "confusion_edhmm", confusmat(CLASSES, int64(data.z), int64(result)))
    write(file, "confusion_rf", confusmat(CLASSES, int64(data.z), int64(predictions)))
    close(file);
end
